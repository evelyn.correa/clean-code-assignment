import './App.css';
import { useState, useEffect } from 'react';
import ItemCard from '../components/item-card';
import actions from '../actions/index';

function App() {
  const [items, setItems] = useState([]);
  const [item, setItem] = useState('');

  useEffect(() => {
    getItems()
  }, [])

  function handleChange(e) {
    setItem(e.target.value);
  }

  function getItems() {
    actions.getItems().then(data => setItems(data));
  }

  function deleteItem (id) {
    actions.deleteItem(id);
    getItems();
  }

  function addItem () {
    actions.addItem(item).then(data => {
      setItems([...items, data]);
    })
  }

  return (
    <div className="App">
      <h3>To do List</h3>
      <div key="addItem" className="addItem">
          <input 
            placeholder="Insert task to add to list"
            data-testid="item-input"
            value={item}
            onChange={handleChange}
            />
          <button onClick={addItem} data-testid="Add">Add</button>
      </div>

      <div className="App-body" data-testid="itemlist">
        {items.length > 0 && items.map(i => (
          <ItemCard key={i._id} item={i.item} deleteFunction={() => deleteItem(i._id)}/>
        ))}
      </div>
    </div>
  );
}

export default App;
