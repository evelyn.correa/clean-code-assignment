import './itemcard.css';

function ItemCard({ item, deleteFunction }) {
    return (
        <div className="itemcard-wrapper">
            <p className="itemcard-item" data-testid='itemcard-item'>{item}</p>
            <button 
                onClick={deleteFunction} 
                data-testid="itemcard-button"
                className="itemcard-button"
            >
                Delete
            </button>
        </div>
    );
}

export default ItemCard;