const apiUrl = "http://localhost:3001";

const headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
};

const getItems = async() => { 
    return await fetch(`${apiUrl}/`)
        .then(res => res.json())
        .catch(console.error)
}

const deleteItem = async(item) => {
    return await fetch(`${apiUrl}/deleteItem`, {
        method: 'DELETE',
        headers: headers,
        body: JSON.stringify({ _id: item}),
      })
      .then(res => res.text())
      .then(data => alert(data))
      .catch(console.error);
}

const addItem = async(item) => {
    return await fetch(`${apiUrl}/addItem`, {
      method: 'POST',
      headers: headers,
      body: JSON.stringify({ item: item }),
    })
      .then(res => res.json())
      .catch(console.error);
}

const actions = {
    getItems, addItem, deleteItem
}

export default actions;