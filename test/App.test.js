
import { fireEvent, render, screen } from '@testing-library/react';
import App from '../src/views/App';

describe('Main page', () => {
  let wrapper, input, button, deleteButton;

  beforeEach(() => {
    wrapper = render(<App />);
    input = wrapper.getByTestId('item-input');
    fireEvent.change(input, { target: { value: 'Item1'}});

    button = wrapper.getByTestId('Add');
  })
  
  test('Render correctly', () => {
    expect(render(<App />)).toMatchSnapshot();
  });

  test('Change input', () => {
    expect(input.value).toBe('Item1');
  })

  test('Add item in list', () => {
    const itemValue = 'Item1';

    fireEvent.change(input, { target: { value: itemValue}});
    fireEvent.click(button);

    const listContent = wrapper.getByTestId('itemcard-item');

    expect(input.value).toBe('');
    expect(listContent.childNodes[0].textContent).toBe(itemValue);
  });

  test('Remove item in list', () => {
    fireEvent.change(input, { target: { value: 'Item1'}});
    fireEvent.click(button);

    deleteButton = screen.getByTestId('itemcard-button');
    fireEvent.click(deleteButton);

    const listContent = wrapper.getByTestId('itemlist');

    expect(listContent).toMatchSnapshot();
  });
})
