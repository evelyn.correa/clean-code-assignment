const bp = require('body-parser');
const express = require('express');
const { initDB, deleteItem, getItems, addItems } = require('./db-controller');

const PORT = 3001;
const app = express();

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "http://localhost:3000");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
    next();
});

app.use('/addItem', bp.json());
app.use('/deleteItem', bp.json());

app.get("/", async(req, res) => {
    const result = await getItems();
    res.json(result);
})

app.post("/addItem", async(req, res) => {
    await addItems(req.body).then(data => res.send(data));
})

app.delete("/deleteItem", async(req, res) => {
    await deleteItem(req.body._id).then(data => res.send(data));
})

app.listen(PORT, () => {
    console.log(`Server listening on ${PORT}`);
    initDB();
})
