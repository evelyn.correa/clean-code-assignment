const { MongoClient, ObjectId } = require("mongodb");

const uri = "mongodb+srv://app-user:app-password@todo-list.pfzua.mongodb.net/todolist?retryWrites=true&w=majority";
const nameDB = "todo-list";
const nameColl = "todolist";

let database, collection;

const initDB = () => {
    MongoClient.connect(uri, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      }, (error, client) => {
        if(error) {
            throw error;
        }
        database = client.db(nameDB);
        collection = database.collection(nameColl);
        console.log(`Connected to ${nameDB} and ${nameColl}` );
    });
}

const getItems = async () => {
    let dblist = [];
    await collection.find({ }).forEach(r => dblist.push(r));
    return dblist;
}

const addItems = async(item) => {
    let response;
    await collection.insertOne(item, (err, result) => {
        if(err) {
            return { status: 500, err };
        }
         response = result.ops[0];
         console.log("inner", response)
    });
    console.log("outer", response)
    return response
}

const deleteItem = async(_id) => {
    return await collection.deleteOne({ _id: ObjectId(_id)}, (err, result) => {
        console.log(err)
        if(err) {
            return { status: 500, err }
        }
        return `Item ${_id} deleted successfully!`;
    })
    //return response
}

const dbFunctions = {
    initDB,
    getItems,
    addItems,
    deleteItem
};

module.exports = dbFunctions;